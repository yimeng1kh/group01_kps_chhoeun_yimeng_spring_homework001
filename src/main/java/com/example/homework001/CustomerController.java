package com.example.homework001;

import org.apache.coyote.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;


@RestController
@RequestMapping("/api/v1")
public class CustomerController {

    static int nextid=3;

    static ArrayList<Customer> customers= new ArrayList<>();
    public CustomerController(){
        customers.add(new Customer(1,"nara","Male", 19,"PP"));
        customers.add(new Customer(2,"dara","Male", 29,"KPS"));
    }

    // get all Customer
    @GetMapping("/customers")
    public ResponseEntity<?> getAllCustomer(){
        return ResponseEntity.ok(new CustomerResponse<ArrayList<Customer>>(
                 "OK",
                 "Successfully get Customer",
                customers
        ));
    }

    // find customer by id
    @GetMapping("/customers{customerId}")
    public ResponseEntity<?> getCustomerById(@PathVariable("customerId") Integer cusId){
        for(Customer cus: customers){
            if(cus.getId()==cusId){
                return ResponseEntity.ok(new CustomerResponse<>("OK", "This record has found successfully",cus));
            }
        }
        return ResponseEntity.notFound().build();
    }

    //find customer by name
    @GetMapping("/customers/search")
    public ResponseEntity<?> findCustomerByName(@RequestParam String name){
        for(Customer cus: customers){
            if(cus.getName().equals(name)){
                return ResponseEntity.ok(new CustomerResponse<>("OK", "This record has found successfully",cus)
                );
            }
        }
        return ResponseEntity.notFound().build();
    }

    // delete customer by id
    @DeleteMapping("/customers/{customerId}")
    public ResponseEntity<?> deleteCustomerById(@RequestParam int id, @RequestBody Customer customer){
        for(Customer customer1:customers){
            if(customer1.getId() == id){
                customers.remove(customer1);
                return ResponseEntity.ok(new CustomerResponse<>(
                        "OK",
                        "This record has been delete successfully",
                        null
                ));
            }
        };
        return ResponseEntity.notFound().build();
    }

    // insert to customer
    @PostMapping("/customers")
        public ResponseEntity<?> insertCustomer(@RequestBody CustomerRequest customer){
        Customer c = new Customer();
        customers.add(new Customer(nextid, customer.getName(), customer.getGender(), customer.getAge(), customer.getAddress()));
        c.setId(nextid);
        c.setName(customer.getName());
        c.setGender(customer.getAddress());
        c.setAge(customer.getAge());
        c.setAddress(customer.getAddress());
        nextid++;
        return ResponseEntity.ok(new CustomerResponse<>(
                "OK",
                "This record is Created successfully", c));
    }

     //update customer
    @PutMapping("/customers")
    public ResponseEntity<?> updateCustomer(@RequestParam int id, @RequestBody  Customer customer){
        for(Customer customer1:customers){
            if(customer1.getId() == id){
                //customer.setId(nextid);
                customer1.setName(customer.getName());
                customer1.setGender(customer.getGender());
                customer1.setAge(customer.getAge());
                customer1.setAddress(customer.getAddress());
                return ResponseEntity.ok(new CustomerResponse<>("OK", "You're updated Successfully", customer1));
            }
        };
        return ResponseEntity.notFound().build();
    }

}
