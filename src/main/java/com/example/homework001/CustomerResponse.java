package com.example.homework001;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.time.DateTimeException;
import java.time.LocalDateTime;
import java.util.Date;
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomerResponse <T>{
    private String message;


    public Date getDateTime() {
        return dateTime;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getLoad() {
        return load;
    }

    public void setLoad(T load) {
        this.load = load;
    }

    public CustomerResponse( String status, String message, T load) {
        this.dateTime = new Date();
        this.status = status;
        this.message = message;
        this.load = load;
    }

    private T load;
    private String status;
    private Date dateTime;
}
